from flask import Blueprint, request

from models import Experiences, Ru, Content, Terms


experiences_app = Blueprint('experiences', __name__)


@experiences_app.route('/')
def get_all_experiences():
    return Experiences.objects.exclude("id").to_json()

@experiences_app.route('/<id>')
def get_experience_by_id(id):
    experience = Experiences.objects.exclude("id").get(experience_id=id)
    return experience.to_json()

@experiences_app.route('/', methods=['POST'])
def create_new_experience():
    Experiences(
        name=request.form.get('name'), 
        title_id=request.form.get('title_id'),
        img=request.form.get('img'),
        terms=Terms(
            start=request.form.get('start'),
            end=request.form.get('end')
        ),
        location_id=request.form.get('location_id'), 
        is_remote=request.form.get('is_remote') == 'true',
        content=Content(
            ru=Ru(
                about_project=request.form.get('about_project'), 
                my_role=request.form.get('my_role')
            )
        ),
        technologies=request.form.get('technologies')
    ).save()   
    return 'ok'

@experiences_app.route('/<id>', methods=['PATCH'])
def edit_experience(id):
    Experiences.objects(experience_id=id).update(
        name=request.form.get('name'), 
        title_id=request.form.get('title_id'),
        img=request.form.get('img'),
        terms=Terms(
            start=request.form.get('start'),
            end=request.form.get('end')
        ),
        location_id=request.form.get('location_id'), 
        is_remote=request.form.get('is_remote') == 'true',
        content=Content(
            ru=Ru(
                about_project=request.form.get('about_project'), 
                my_role=request.form.get('my_role')
            )
        ),
        technologies=request.form.get('technologies'))
    return 'ok'

@experiences_app.route('/<id>', methods=['DELETE'])
def delete_experience(id):
    Experiences.objects(experience_id=id).delete()
    return 'ok'