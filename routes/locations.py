from flask import Blueprint, request

from models import Locations


locations_app = Blueprint('locations', __name__)


@locations_app.route('/')
def get_all_locations():
    return Locations.objects.exclude("id").to_json()

@locations_app.route('/<id>')
def get_location_by_id(id):
    location = Locations.objects.exclude("id").get(location_id=id)
    return location.to_json()

@locations_app.route('/', methods=['POST'])
def create_new_location():
    Locations(
        hh_id=request.form.get('hh_id'),
        name=request.form.get('name')
    ).save()
    return 'ok'

@locations_app.route('/<id>', methods=['PATCH'])
def edit_location(id):
    Locations.objects(location_id=id).update(
        hh_id=request.form.get('hh_id'),
        name=request.form.get('name')
    )
    return 'ok'

@locations_app.route('/<id>', methods=['DELETE'])
def delete_location(id):
    Locations.objects(location_id=id).delete()
    return 'ok'