from flask import Blueprint, request

from models import Persons, Contacts, Langs, Education, Higher, Phone


persons_app = Blueprint('persons', __name__)


@persons_app.route('/')
def get_all_persons():
    return Persons.objects.exclude("id").to_json()

@persons_app.route('/<id>')
def get_person_by_id(id):
    person = Persons.objects.exclude("id").get(person_id=id)
    print(person)
    return person.to_json()

@persons_app.route('/', methods=['POST'])
def create_new_person():
    Persons(
        first_name=request.form['first_name'],
        last_name=request.form.get('last_name'),
        gender=request.form.get('gender'),
        contacts=Contacts(
            phone=Phone(
                country=request.form.get('country'),
                city=request.form.get('city'),
                number=request.form.get('number'),
                formatted=request.form.get('formatted')
            ),
            phone_comment=request.form.get('phone_comment'),
            email=request.form.get('email'),
            links=request.form.getlist('links')
        ),
        hh_photo_id=request.form.get('hh_photo_id'),
        langs=Langs(
            ru=request.form.get('ru'),
            en=request.form.get('en'),
            es=request.form.get('es'),
            de=request.form.get('de'),
            ua=request.form.get('ua'),
            rs=request.form.get('rs'),
            bg=request.form.get('bg')
        ),
        education=Education(
            level=request.form.get('level'),
            higher=Higher(
                year_finish=request.form.get('year_finish'),
                name=request.form.get('name'),
                hh_id=request.form.get('hh_id')
            ))
    ).save()
    return 'ok'

@persons_app.route('/<id>', methods=['PATCH'])
def edit_person(id):
    Persons.objects(person_id=id).update(
        first_name=request.form['first_name'],
        last_name=request.form.get('last_name'),
        gender=request.form.get('gender'),
        contacts=Contacts(
            phone=Phone(
                country=request.form.get('country'),
                city=request.form.get('city'),
                number=request.form.get('number'),
                formatted=request.form.get('formatted')
            ),
            phone_comment=request.form.get('phone_comment'),
            email=request.form.get('email'),
            links=request.form.getlist('links')
        ),
        hh_photo_id=request.form.get('hh_photo_id'),
        langs=Langs(
            ru=request.form.get('ru'),
            en=request.form.get('en'),
            es=request.form.get('es'),
            de=request.form.get('de'),
            ua=request.form.get('ua'),
            rs=request.form.get('rs'),
            bg=request.form.get('bg')
        ),
        education=Education(
            level=request.form.get('level'),
            higher=Higher(
                year_finish=request.form.get('year_finish'),
                name=request.form.get('name'),
                hh_id=request.form.get('hh_id')
            ))
    )
    return 'ok'

@persons_app.route('/<id>', methods=['DELETE'])
def delete_person(id):
    Persons.objects(person_id=id).delete()
    return 'ok'