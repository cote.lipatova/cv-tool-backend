from flask import Blueprint, request

from models import Jobs, KeySkills


jobs_app = Blueprint('jobs', __name__)


@jobs_app.route('/')
def get_all_jobs():
    return Jobs.objects.exclude("id").to_json()

@jobs_app.route('/<id>') 
def get_job_by_id(id):
    print(request.form)
    job = Jobs.objects.exclude("id").get(job_id=id)

    print(request.form)
    return job.to_json()

@jobs_app.route('/', methods=['POST'])
def create_new_job():
    print(request.form)
    Jobs(
        title=request.form.get('title'),
        keySkills=KeySkills(
            categoryName=request.form.get('categoryName'),
            skills=request.form.getlist('skills')
        ),
        about=request.form.get('about'),
        hh_specializations=request.form.getlist('hh_specializations')
    ).save()
    return 'ok'


@jobs_app.route('/<id>', methods=['PATCH'])
def edit_job(id):
    Jobs.objects(job_id=id).update(
        title=request.form.get('title'),
        keySkills=KeySkills(
            categoryName=request.form.get('categoryName'),
            skills=request.form.getlist('skills')
        ),
        about=request.form.get('about'),
        hh_specializations=request.form.getlist('hh_specializations')
    )
    return 'ok'

@jobs_app.route('/<id>', methods=['DELETE'])
def delete_job(id):
    Jobs.objects(job_id=id).delete()
    return 'ok'