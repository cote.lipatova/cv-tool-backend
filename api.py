from flask import Flask, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

from routes.experiences import experiences_app
from routes.jobs import jobs_app
from routes.locations import locations_app
from routes.persons import persons_app

app.register_blueprint(experiences_app, url_prefix='/experiences')
app.register_blueprint(jobs_app, url_prefix='/jobs')
app.register_blueprint(locations_app, url_prefix='/locations')
app.register_blueprint(persons_app, url_prefix='/persons')

import generator.preset as generator_models 
from models import Jobs, Persons, Locations, Experiences

@app.route('/deploy/hh/', methods=['POST'])
def deploy_to_hh():
    print('Deploy to HH called')
    return 'OK'

@app.route('/deploy/pdf/', methods=['POST'])
def deploy_to_pdf():
    print('Deploy to pdf called')
    #job_id = request.form['job_id']
    #person_id = request.form['person_id']
    #location_id = request.form['location_id']
    #pdf_type = request.form['pdf-type']
    #experiences_ids = request.form.getlist('experiences')

    #job = Jobs.objects.get(job_id=job_id)
    #person = Persons.objects.get(person_id=person_id)
    #location = Locations.objects.get(location_id=location_id)

    #experiences = []
    #for experience_id in experiences_ids:
        #experience = Experiences.objects.get(experience_id=experience_id)
        #experiences.append(experience)

    #preset = generator_models.Preset(
        #person=generator_models.Person(
            #first_name=person.first_name,
            #last_name=person.last_name,
            #gender=person.gender,
            #contacts=generator_models.Contacts(
                #phone=generator_models.Phone(
                    #country=person.contacts.phone.country,
                    #city=person.contacts.phone.city,
                    #number=person.contacts.phone.number,
                    #comment=person.contacts.phone_comment
                #),
                #email=person.contacts.email, 
                #links=person.contacts.links
            #),
            #hh_photo_id=person.hh_photo_id,
            #langs=person.langs,
            #education=person.education
        #),
        #location=generator_models.Location(
            #hh_id=location.hh_id,
            #name=location.location_name,
        #),
        #job=generator_models.Job(
            #title=job.title,
            #key_skills=job.key_skills,
            #about=job.about,
            #hh_specializations=job.hh_specializations 
        #),
        #lang='en', # TODO
        #is_remote=False, # TODO
        #experiences=[generator_models.Experience(
            #name=experience.name,
            #img=experience.img,
            #terms_start=experience.terms.start,
            #terms_end=experience.terms.end,
            #title_id=experience.title_id,
            #location_id=experience.location_id,
            #is_remote=experience.is_remote,
            #about_project=experience.content.ru.about_project, 
            #my_role=experience.content.ru.my_role,
            #technologies=experience.technologies,
            #lang='en' # TODO
        #) for experience in experiences]
    #)

    #pdf_generator.generate(preset, pdf_type)
    
    return 'OK'