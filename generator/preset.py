import re
import datetime
from pprint import pprint

from .tools import load_json
from .exc import UnexpectedOptionError, SmartKeyErrorHandling

class Phone(object):
    def __init__(self, country, city, number, comment=''):
        self.country = country
        self.city = city
        self.number = number
        self.comment = comment

        self.formatted = self.format_phone(country, city, number)

    @staticmethod
    def format_phone(country, city, number):
        city_groups = re.findall('...', city) # Split by 3 
        number_groups = re.findall('..?', number) # Split by 2

        if len(number_groups[-1]) == 1:
            number_groups = []
            number_groups.append(number[0:3])
            number_groups += re.findall('..', number[3:])

        city_formatted = '-'.join(city_groups)
        number_fomatted = '-'.join(number_groups)

        return f'+{country} ({city_formatted}) {number_fomatted}'

class Contacts(object):
    def __init__(self, phone, email, links):
        self.phone = phone
        self.email = email
        self.links = links

    def for_hh(self):
        return [
            {
                "type": {"id": "cell"},
                "value": {
                    "country": self.phone.country,
                    "city": self.phone.city,
                    "number": self.phone.number,
                    "formatted": self.phone.formatted
                },
                "comment": self.phone.comment,
                "preferred": False
            },
            {
                "type": {"id": "email"},
                "value": self.email,
                "preferred": True
            }
        ]

class Title(object):
    def __init__(self, name):
        self.name = name

    @classmethod
    def load_by_id(cls, title_id, lang):
        titles = load_json('data/titles.json')

        try:
            title_data = titles[title_id]
        except KeyError: 
            raise UnexpectedOptionError(title_id, titles.keys())

        name = title_data[lang]

        return cls(name=name)

class Experience(object):
    def __init__(
        self, 
        name, 
        img, 
        terms_from,
        terms_to,
        title,
        location, 
        is_remote,
        about_project, 
        my_role, 
        technologies,
        lang
    ): 
        self.name = name
        self.img = img
        self.terms_from = terms_from
        self.terms_to = terms_to
        self.title = title
        self.location = location
        self.is_remote = is_remote
        self.about_project = about_project
        self.my_role = my_role
        self.technologies = technologies
        self.lang = lang 

    @staticmethod
    def __clean_html(str):
        str = re.sub('<a href="([^"]+)">', '', str)
        str = re.sub('</a>', '', str)
        str = re.sub('<br ?/>', '\n', str)

        return str

    def for_hh(self):
        return {
            "company": self.name,
            "area": {"id": self.location.hh_id },
            "position": self.title.name,
            "start": self.terms_from.strftime('%Y-%m-%d'),
            "end": self.terms_to.strftime('%Y-%m-%d'),
            "description": self.simple_text_description()
        } 

    def simple_text_description(self):
        assert self.lang in ['ru'], 'Translation is not provided'

        if self.lang == 'ru':
            return '\n'.join((
                'О проекте:',
                self.__clean_html(self.about_project),
                '',
                'Моя роль:',
                self.__clean_html(self.my_role),
                '',
                'Технологии:',
                self.technologies
            ))

    @classmethod
    def load_by_id(cls, experience_id, lang):
        experiences = load_json('data/experiences.json')

        try:
            experience_data = experiences[experience_id]
        except KeyError:
            raise UnexpectedOptionError(experience_id, experiences.keys())

        with SmartKeyErrorHandling(experience_data=experience_data): 
            name = experience_data['name']
            img = experience_data['img']
            terms_from_str = experience_data['terms']['start']
            terms_to_str = experience_data['terms']['end']
            title_id = experience_data['title_id']
            location_id = experience_data['location_id']
            is_remote = experience_data['is_remote']
            content = experience_data['content']
            technologies = experience_data['technologies'] 

        terms_from = datetime.datetime.strptime(terms_from_str, '%Y-%m-%d')
        terms_to = datetime.datetime.strptime(terms_to_str, '%Y-%m-%d')

        title = Title.load_by_id(title_id, lang)
        location = Location.load_by_id(location_id, lang)
        
        localized_content = content[lang]

        about_project = localized_content['about_project']
        my_role = localized_content['my_role']

        return cls(
            name=name,
            img=img,
            terms_from=terms_from,
            terms_to=terms_to,
            title=title,
            location=location,
            is_remote=is_remote,
            about_project=about_project,
            my_role=my_role,
            technologies=technologies,
            lang=lang
        )



class Person(object):
    def __init__(self, first_name, last_name, gender, contacts, hh_photo_id, langs, education):
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.contacts = contacts
        self.hh_photo_id = hh_photo_id 
        self.langs = langs
        self.education = education

    @classmethod
    def load_by_id(cls, person_id, lang):
        persons = load_json('data/persons.json')

        try:
            person_data = persons[person_id]
        except KeyError:
            raise UnexpectedOptionError(person_id, persons.keys())

        with SmartKeyErrorHandling(person_data=person_data):
            first_name_i18n = person_data['first_name']
            last_name_i18n = person_data['last_name']
            gender = person_data['gender']
            contacts_dict = person_data['contacts']
            hh_photo_id = person_data['hh_photo_id']
            langs = person_data['langs']
            education = person_data['education']  

        with SmartKeyErrorHandling(first_name_i18n=first_name_i18n):
            first_name = first_name_i18n[lang]

        with SmartKeyErrorHandling(last_name_i18n=last_name_i18n):
            last_name = last_name_i18n[lang]

        with SmartKeyErrorHandling(contacts_dict=contacts_dict): 
            contacts_phone = contacts_dict['phone']
            contacts_phone_comment = contacts_dict['phone_comment']
            contacts_email = contacts_dict['email']
            contacts_links = contacts_dict['links']

        with SmartKeyErrorHandling(contacts_phone=contacts_phone):
            contacts_phone_country = contacts_phone['country']
            contacts_phone_city = contacts_phone['city']
            contacts_phone_number = contacts_phone['number']

        phone = Phone(
            country=contacts_phone_country,
            city=contacts_phone_city,
            number=contacts_phone_number,
            comment=contacts_phone_comment
        )

        contacts = Contacts(
            phone=phone,
            email=contacts_email,
            links=contacts_links
        )

        return cls(
            first_name=first_name,
            last_name=last_name,
            gender=gender,
            contacts=contacts,
            hh_photo_id=hh_photo_id,
            langs=langs,
            education=education
        )

    def hh_education(self):
        return {
            "level": {"id": self.education['level']},
            "primary": [
                {
                    "name": education['name'],
                    "year": education['year_finish'],
                    "name_id": education['hh_id']
                } for education in self.education['higher']
            ]
        }

    def hh_langs(self):
        lang_abbr_to_hh_id = {
            'ru': 'rus',
            'en': 'eng',
            'es': 'spa',
            'de': 'deu',
            'ua': 'ukr',
            'rs': 'srp',
            'bg': 'bul'
        }

        level_name_to_code = {
            'native': 'c2',
            'fluent': 'c2',
            'advanced': 'c1',
            'intermediate': 'b2',
            'beginner': 'a2'
        }

        return [{
            "id": lang_abbr_to_hh_id[code],
            "level": { "id": level_name_to_code[level] }
        } for code, level in self.langs.items() if code != 'ru']


class Location(object):
    def __init__(self, hh_id, name):
        self.hh_id = hh_id
        self.name = name

    @classmethod
    def load_by_id(cls, location_id, lang):
        locations = load_json('data/locations.json')

        try:
            location_data = locations[location_id]
        except KeyError:
            raise UnexpectedOptionError(location_id, locations.keys())

        with SmartKeyErrorHandling(location_data=location_data): 
            hh_id = location_data['hh_id']
            name_i18n = location_data['name'] 

        with SmartKeyErrorHandling(name_i18n=name_i18n):
            name = name_i18n[lang]

        return cls(hh_id=hh_id, name=name)


class Job(object):
    def __init__(self, title, key_skills, about, hh_specializations):
        self.title = title
        self.key_skills = key_skills
        self.about = about
        self.hh_specializations = hh_specializations

    @classmethod
    def load_by_id(cls, job_id, lang):
        jobs = load_json('data/jobs.json')

        try:
            job_data = jobs[job_id]
        except KeyError:
            raise UnexpectedOptionError(job_id, jobs.keys())

        with SmartKeyErrorHandling(job_data=job_data):
            title_i18n = job_data['title']
            key_skills = job_data['key_skills']
            about_i18n = job_data['about']
            hh_specializations = job_data['hh_specializations']

        with SmartKeyErrorHandling(about_i18n=about_i18n):
            about = about_i18n[lang]

        with SmartKeyErrorHandling(title_i18n=title_i18n):
            title = title_i18n[lang]

        return cls(
            title=title, 
            key_skills=key_skills,
            about=about,
            hh_specializations=hh_specializations
        )

class Preset(object):
    def __init__(self, person, location, job, lang, is_remote, experiences):
        self.person = person
        self.location = location
        self.job = job
        self.experiences = experiences
        self.lang = lang
        self.is_remote = is_remote

    @classmethod
    def load_by_id(cls, preset_id):
        presets = load_json('data/presets.json')

        try:
            preset_data = presets[preset_id]
        except KeyError:
            raise UnexpectedOptionError(preset_id, presets.keys())

        with SmartKeyErrorHandling(preset_data=preset_data):
            person_id = preset_data['person_id']
            location_id = preset_data['location_id']
            job_id = preset_data['job_id']
            lang = preset_data['lang']
            is_remote = preset_data['is_remote']
            experiences_ids = preset_data['experiences_ids']

        person = Person.load_by_id(person_id, lang)
        location = Location.load_by_id(location_id, lang)
        job = Job.load_by_id(job_id, lang)
        
        experiences = []

        for experience_id in experiences_ids:
            try:
                experience = Experience.load_by_id(experience_id, lang=lang)
            except Exception as e:
                raise Exception(f'While loading experience "{experience_id}": {e}')

            experiences.append(experience)

        return cls(
            person=person,
            location=location,
            job=job,
            lang=lang,
            is_remote=is_remote,
            experiences=experiences
        )

    def check_for_hh(self):
        assert len(self.job.key_skills) <= 30, f'Max count of key skills is 30 ({len(key_skills)} provided'
        
        experiences_durations = self.calc_experiences_durations()
        total_experience_months = sum(map(lambda item: item[1], experiences_durations)) 

        if total_experience_months > 6 * 12:
            print(f'WARNING: Total experience is {total_experience_months} ({round(total_experience_months / 12, 1)} years). That more than 6 years (drammaticly lower market)')
            for experience_name, duration_months in experiences_durations:
                print(f'{experience_name}: {duration_months} months')

        assert len(set([spec.split('.')[0] for spec in self.job.hh_specializations])) == 1, 'HH API doesn\'t allow to set specialities from the differen areas (e.g. markering (3.xxx) and development(1.xxx)' 

    def calc_experiences_durations(self):
        durations = []

        for experience in self.experiences:
            diff = experience.terms_to - experience.terms_from
            diff_months = round(diff.days / 30)

            durations.append((experience.name, diff_months))

        return durations

    def get_hh_title(self):
        return f'{self.job.title} ({self.location.name})'

    def hh_data(self):
        return {
            'title': self.get_hh_title(),
            'gender': { 'id': self.person.gender },
            'area': { 'id': self.location.hh_id },
            'experience': [experience.for_hh() for experience in self.experiences],
            'skill_set': self.job.key_skills,
            "citizenship": {"id": 113}, # Russian Federation
            "relocation": { "type": {"id": 'relocation_possible' } },
            "skills": self.job.about,
            "education": self.person.hh_education(),
            "language": self.person.hh_langs(),
            "photo": { "id": self.person.hh_photo_id },
            "specialization": [{"id": id} for id in self.job.hh_specializations],
            "schedule": [{"id": id} for id in ['fullDay', 'flexible', 'remote']],
            "employments": [{"id": id} for id in ['full', 'part', 'project']],
            "contact": self.person.contacts.for_hh()
        }