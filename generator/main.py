import locale
from argparse import ArgumentParser

import settings
import pdf_generator
from integrations.hh.create_resume import HHClient
from preset import Preset

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('PRESET_NAME')
    
    subparsers = parser.add_subparsers(dest='MODE')
    subparsers.required = True

    parser_pdf = subparsers.add_parser('pdf')
    parser_pdf.add_argument('TEMPLATE_NAME')

    parser_hh = subparsers.add_parser('hh')
    parser_hh.add_argument('ACCESS_TOKEN')

    args = parser.parse_args()

    # Code starts here:

    preset = Preset.load_by_id(args.PRESET_NAME)

    locale.setlocale(locale.LC_ALL, preset.lang)

    if args.MODE == 'pdf':
        pdf_generator.generate(preset, args.TEMPLATE_NAME)

    elif args.MODE == 'hh':
        preset.check_for_hh()

        hh_client = HHClient(args.ACCESS_TOKEN)
        hh_client.create_cv(preset.hh_data())

        print('Successfully finished')
    else:
        raise Exception(f'Unsupported mode: {mode}')