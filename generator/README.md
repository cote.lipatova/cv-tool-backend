# CV generator

## Installation

```pip install -r requirements.txt```

Install wkhtmltopdf: 
- Download suitable version from [this page](https://wkhtmltopdf.org/downloads.html)
- Run the installer
- "I agree" -> "Install" -> "Close" (when progress bar will be at the 100%)