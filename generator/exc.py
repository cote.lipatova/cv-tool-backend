from pprint import pprint

class UnexpectedOptionError(Exception):
    def __init__(self, value, options):
        options_str = ', '.join(options)
        super().__init__(f'Unexpected option: {value}. Possible options: {options_str}')

class SmartKeyErrorHandling(object):
    def __init__(self, **kw):
        assert len(kw) == 1, 'You may pass the only one argument to the handler - iterated dict'
        self.dict_name = list(kw.keys())[0]
        self.dict_content = kw[self.dict_name]

    def __enter__(self):
        return True
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            if exc_type.__name__ == 'KeyError':
                print(f'{self.dict_name} content:')
                pprint(self.dict_content)