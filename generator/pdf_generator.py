import json
import base64
import os
from pathlib import Path

import pdfkit 
import jinja2
import sass

import settings
from exc import UnexpectedOptionError

def file_to_base64(filepath):
    with open(filepath, 'rb') as f: 
        return base64.b64encode(f.read()).decode()

def load_templates():
    # Configurate templates loader
    loader = jinja2.FileSystemLoader(searchpath=settings.TEMPLATES_DIR)
    templates_env = jinja2.Environment(loader=loader)
    templates_env.globals['static'] = file_to_base64
    templates_env.globals['static_png'] = lambda filepath: 'data:image/png;base64,' + file_to_base64(filepath)
    templates_env.globals['static_otf'] = lambda filepath: 'data:font/opentype;base64,' + file_to_base64(filepath)
    templates_env.globals['static_woff'] = lambda filepath: 'data:application/font-woff;base64,' + file_to_base64(filepath)
    templates_env.globals['sass_compile'] = lambda sass_code: sass.compile(string=sass_code)

    # Load required templates
    templates = {}

    for name, path in settings.TEMPLATES.items():
        templates[name] = templates_env.get_template(path)
    
    return templates

def load_tranlslations():
    translations = {}

    for lang_code, translations_file in settings.TRANSLATIONS.items():
        with open(translations_file, encoding='utf-8') as f:
            try:
                translations[lang_code] = json.load(f)
            except json.decoder.JSONDecodeError as e:
                raise Exception(f'On file {translations_file}: {e}')

    return translations

def load_skills():
    with open('skills.json') as f:
        skills = json.load(f)

    return skills

def render_pdf(template, lang, title, translations, skills, out_file, additional_vars): 
    html = template.render(
        lang=lang,
        cv_title=title,
        skills=skills,
        **translations,
        **additional_vars,
    )
    
    # with open(f'debug_{lang}.html', 'w', encoding='utf-8') as f: 
    #     f.write(html)

    dirname = os.path.dirname(out_file)

    if not os.path.exists(dirname):
        os.makedirs(dirname)

    pdfkit.from_string(
        html, 
        out_file,
        configuration=pdfkit.configuration(wkhtmltopdf=settings.WKHTMLTOPDF_EXECUTABLE)
    )

def generate(preset, template_name, out_file=None, additional_vars={}):
    templates = load_templates()

    try:
        template = templates[template_name]
    except KeyError:
        raise UnexpectedOptionError(template_name, templates.keys())

    if out_file is None:
        out_file = str(Path('.') / 'out' / f'{template_name}.pdf')

    render_pdf( 
        template=template,
        title=preset.job.title,
        lang=preset.lang,
        translations={
            'first_name': preset.person.first_name,
            'last_name': preset.person.last_name,
            'is_remote': preset.is_remote,
            'contacts': [
                preset.person.contacts.phone.formatted,
                preset.person.contacts.email,
                *preset.person.contacts.links
            ],
            'key_skills': preset.job.key_skills,
            'about': preset.job.about,
            'experiences': preset.experiences,
            'langs': preset.person.langs
        },
        skills=preset.job.key_skills,
        out_file=out_file,
        additional_vars=additional_vars
    )