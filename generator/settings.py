WKHTMLTOPDF_EXECUTABLE = 'C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'

TEMPLATES_DIR = './templates'
TEMPLATES = {
    'full': 'cv_full/cv_full.jinja',
    'simple': 'cv_simple/cv_simple.jinja'
}

TRANSLATIONS = {
    'en': 'translations/en.json', 
    'ru': 'translations/ru.json', 
    'es': 'translations/es.json',
    'zh': 'translations/zh.json', 
    'de': 'translations/de.json',
    'fr': 'translations/fr.json',
}