import json

def load_json(filename):
    '''
    Load JSON with utf-8 and improved error handling
    '''

    with open(filename, encoding='utf-8') as f:
        try:
            data = json.load(f)
        except json.decoder.JSONDecodeError as e:
            raise Exception(f'On file {filename}: {e}')

    return data