from mongoengine import *

connect('cv-tool')

# Experiences

class Ru(EmbeddedDocument):
    about_project = StringField()
    my_role = StringField()

class Content(EmbeddedDocument):
    ru = EmbeddedDocumentField(Ru)

class Terms(EmbeddedDocument):
    start = StringField()
    end = StringField()

class Experiences(Document):
    experience_id = SequenceField(required=True)
    name = StringField()
    title_id = StringField()
    img = StringField()
    terms = EmbeddedDocumentField(Terms)
    location_id = StringField()
    is_remote = BooleanField()
    content = EmbeddedDocumentField(Content)
    technologies = StringField()

# Jobs

class KeySkills(EmbeddedDocument):
    categoryName = StringField()
    skills = ListField(StringField())

class Jobs(Document):
    job_id = SequenceField(required=True)
    title = StringField()
    keySkills = EmbeddedDocumentField(KeySkills)
    about = StringField()
    hh_specializations = ListField(StringField())

# Locations

class Locations(Document):
    location_id = SequenceField(required=True)
    hh_id = IntField()
    name = StringField()

# Persons

class Phone(EmbeddedDocument):
    country = StringField()
    city = StringField()
    number = StringField()
    formatted = StringField()

class Contacts(EmbeddedDocument):
    phone = EmbeddedDocumentField(Phone)
    phone_comment = StringField()
    email = EmailField()
    links = ListField(StringField())

class Langs(EmbeddedDocument):
    ru = StringField()
    en = StringField()
    es = StringField()
    de = StringField()
    ua = StringField()
    rs = StringField()
    bg = StringField()

class Higher(EmbeddedDocument):      
    year_finish = IntField()
    name  = StringField()
    hh_id = IntField()

class Education(EmbeddedDocument):
    level = StringField()
    higher = EmbeddedDocumentField(Higher)

class Persons(Document):
    person_id = SequenceField(required=True)
    first_name = StringField()
    last_name = StringField()
    gender = StringField()
    contacts = EmbeddedDocumentField(Contacts)
    hh_photo_id = IntField()
    langs = EmbeddedDocumentField(Langs)
    education = EmbeddedDocumentField(Education)



